# ORID



## Objective

- I have been working on a small project of parking lot all day. This project combines the ideas of TDD and OOP, and realizes new requirements step by step by constantly updating requirements and rewriting test case and functions. In this process, I feel that the need to go further and further round, a little difficult to understand.



## Reflective

- I sense a little difficulty

## Interpretative

- Since I was first exposed to the idea of TDD, it has changed my programming habits. It is necessary to write test cases before implementing specific functions, and development is driven by tests. The change in programming method makes me difficult to accept at the moment. At the same time, constantly increasing requirements requires me to have a deeper understanding of O-O-P ideas, and then combine TDD and OOP to better complete this small project.

## Decision

- I will continue to learn the ideas of TDD, so that my programming habits can be converted into the mode of TDD, and I will continue to learn the ideas of OOP, so that I can use these two ideas more skillfully.

 