package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_ticket_and_parked_larger_available_position_rate_when_park_given_two_parking_lot_with_different_rate_super_smart_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(20, 20);
        for (int i = 0; i < 5; i++) {
            parkingLot1.park(new Car(i));
        }
        ParkingLot parkingLot2 = new ParkingLot(15, 15);
        for (int i = 5; i < 8; i++) {
            parkingLot2.park(new Car(i));
        }
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(99);
        //when
        ParkingTicket ticket = superSmartParkingBoy.park(car);
        //then
        assertEquals(15, parkingLot1.getEmptyPosition());
        assertEquals(11, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_ticket_park_the_first_parking_lot_when_park_given_super_smart_boy_and_two_parking_lot_with_same_available_position_rate_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy SuperSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        //when
        SuperSmartParkingBoy.park(car);
        //then
        assertEquals(9, parkingLot1.getEmptyPosition());
        assertEquals(10, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_ticket_and_parked_the_second_parking_lot_when_park_given_two_parking_lot_and_a_full_and_super_smart_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car(i));
        }
        Car car = new Car(11);

        //when
        superSmartParkingBoy.park(car);

        //then
        assertEquals(9, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_right_car_each_ticket_when_fetch_given_two_parking_lot_with_parked_a_car_and_super_smart_boy_and_two_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        ParkingTicket ticket1 = parkingLot1.park(car1);
        Car car2 = new Car(2);
        ParkingTicket ticket2 = parkingLot2.park(car2);
        //when
        Car fetchCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchCar2 = superSmartParkingBoy.fetch(ticket2);
        //then
        assertEquals(car1.getId(), fetchCar1.getId());
        assertEquals(car2.getId(), fetchCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_parking_lot_and_super_smart_boy_and_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        // unrecognized ticket
        ParkingTicket wrongTicket = new ParkingTicket(new Car(999));
        //when
        //then
        String expected = "Unrecognized parking ticket";
        UnrecogizedParkingTicketException unRecognizedParkingTicketException1 = assertThrows(UnrecogizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(wrongTicket));
        assertEquals(expected, unRecognizedParkingTicketException1.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_parking_lot_and_super_smart_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        ParkingTicket ticket = superSmartParkingBoy.park(car);
        // used ticket
        superSmartParkingBoy.fetch(ticket);
        //when
        //then
        UnrecogizedParkingTicketException unrecogizedParkingTicketException = assertThrows(UnrecogizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecogizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_two_parking_lot_and_super_smart_boy_and_both_without_position_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10,10);
        ParkingLot parkingLot2 = new ParkingLot(15,15);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < 25; i++) {
            superSmartParkingBoy.park(new Car(i));
        }
        Car car = new Car(99);

        //when
        //then
        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(car));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
