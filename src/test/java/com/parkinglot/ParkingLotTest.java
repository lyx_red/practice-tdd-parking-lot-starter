package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket ticket = parkingLot.park(car);

        //when
        Car fetchCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_tight_car_with_each_ticket_when_fetch_given_parking_lot_and_two_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);

        //when
        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car1.getId(), fetchCar1.getId());
        assertEquals(car2.getId(), fetchCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        // wrong ticket
        ParkingTicket wrongTicket = new ParkingTicket(new Car(999));
        //when
        //then
        String expected = "Unrecognized parking ticket";
        UnrecogizedParkingTicketException unRecognizedParkingTicketException1 = assertThrows(UnrecogizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        assertEquals(expected, unRecognizedParkingTicketException1.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(1);
        ParkingTicket ticket = parkingLot.park(car);
        // used ticket
        parkingLot.fetch(ticket);
        //when
        //then
        UnrecogizedParkingTicketException unrecogizedParkingTicketException = assertThrows(UnrecogizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecogizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_parking_lot_and_without_position_for_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < 10; i++) {
            parkingLot.park(new Car(i));
        }
        Car car = new Car(11);

        //when
        //then
        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

}
