package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    void should_return_ticket_park_more_position_when_park_given_smart_boy_and_two_parking_lot_with_5_position_and_with_10_position_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        for (int i = 0; i < 5; i++) {
            parkingLot1.park(new Car(i));
        }
        parkingLot1.setEmptyPosition(10 - parkingLot1.getCarsList().size());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(6);
        //when
        smartParkingBoy.park(car);
        //then
        assertEquals(5, parkingLot1.getEmptyPosition());
        assertEquals(9, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_ticket_park_the_first_parking_lot_when_park_given_smart_boy_and_two_parking_lot_with_same_position_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        //when
        smartParkingBoy.park(car);
        //then
        assertEquals(9, parkingLot1.getEmptyPosition());
        assertEquals(10, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_ticket_and_parked_the_second_parking_lot_when_park_given_two_parking_lot_and_a_full_and_smart_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car(i));
        }
        Car car = new Car(11);

        //when
        smartParkingBoy.park(car);

        //then
        assertEquals(9, parkingLot2.getEmptyPosition());
    }

    @Test
    void should_return_right_car_each_ticket_when_fetch_given_two_parking_lot_with_parked_a_car_and_smart_boy_and_two_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        ParkingTicket ticket1 = parkingLot1.park(car1);
        Car car2 = new Car(2);
        ParkingTicket ticket2 = parkingLot2.park(car2);
        //when
        Car fetchCar1 = smartParkingBoy.fetch(ticket1);
        Car fetchCar2 = smartParkingBoy.fetch(ticket2);
        //then
        assertEquals(car1.getId(), fetchCar1.getId());
        assertEquals(car2.getId(), fetchCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_parking_lot_and_smart_boy_and_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        // unrecognized ticket
        ParkingTicket wrongTicket = new ParkingTicket(new Car(999));
        //when
        //then
        String expected = "Unrecognized parking ticket";
        UnrecogizedParkingTicketException unRecognizedParkingTicketException1 = assertThrows(UnrecogizedParkingTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));
        assertEquals(expected, unRecognizedParkingTicketException1.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_two_parking_lot_and_smart_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        ParkingTicket ticket = smartParkingBoy.park(car);
        // used ticket
        smartParkingBoy.fetch(ticket);
        //when
        //then
        UnrecogizedParkingTicketException unrecogizedParkingTicketException = assertThrows(UnrecogizedParkingTicketException.class, () -> smartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecogizedParkingTicketException.getMessage());

    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_two_parking_lot_and_smart_boy_and_both_without_position_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < 20; i++) {
            smartParkingBoy.park(new Car(i));
        }
        Car car = new Car(21);

        //when
        //then
        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
