package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_two_parking_lot_and_boy_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        //when
        ParkingTicket ticket = parkingBoy.park(car);
        //then
        assertEquals(1, parkingLot1.getCarsList().size());
        assertEquals(0, parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_ticket_and_parked_the_second_parking_lot_when_pack_given_two_parking_lot_and_a_full_and_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        // parkingLot1 is full
        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car(i));
        }
        Car car = new Car(11);
        //when
        ParkingTicket ticket = parkingBoy.park(car);
        //then
        assertEquals(10, parkingLot1.getCarsList().size());
        assertEquals(1, parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_given_two_parking_lot_and_boy_and_two_ticket() {
        //Given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot1.park(car1);
        ParkingTicket ticket2 = parkingLot2.park(car2);

        //when
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        //then
        assertEquals(car1.getId(), fetchCar1.getId());
        assertEquals(car2.getId(), fetchCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_two_parking_lot_and_boy_and_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        // unrecognized ticket
        ParkingTicket wrongTicket = new ParkingTicket(new Car(999));
        //when
        //then
        String expected = "Unrecognized parking ticket";
        UnrecogizedParkingTicketException unRecognizedParkingTicketException1 = assertThrows(UnrecogizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        assertEquals(expected, unRecognizedParkingTicketException1.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_two_parking_lot_and_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        ParkingTicket ticket = parkingBoy.park(car);
        // used ticket
        parkingBoy.fetch(ticket);
        //when
        //then
        UnrecogizedParkingTicketException unrecogizedParkingTicketException = assertThrows(UnrecogizedParkingTicketException.class, () -> parkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecogizedParkingTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_two_parking_lot_and_boy_and_both_without_position_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car(i));
        }

        for (int i = 10; i < 20; i++) {
            parkingBoy.park(new Car(i));
        }
        Car car = new Car(21);

        //when
        //then
        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}