package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingLot {
    private static final int CAPACITY = 10;

    private int emptyPosition;

    private final int capacity;

    private List<Car> carsList = new ArrayList<>();


    public int getEmptyPosition() {
        return emptyPosition;
    }

    public void setEmptyPosition(int emptyPosition) {
        this.emptyPosition = emptyPosition;
    }


    public int getCapacity() {
        return capacity;
    }


    public List<Car> getCarsList() {
        return carsList;
    }

    public ParkingLot() {
        this(CAPACITY, CAPACITY);
    }

    public ParkingLot(int capacity, int emptyPosition) {
        this.capacity = capacity;
        this.emptyPosition = emptyPosition;
    }

    public ParkingTicket park(Car car) throws NoAvailablePositionException {
        if (getEmptyPosition() > 0) {
            carsList.add(car);
            setEmptyPosition(getEmptyPosition() - 1);
            ParkingTicket parkingTicket = new ParkingTicket(car, false);
            return parkingTicket;
        }
        throw new NoAvailablePositionException("No available position.");
    }

    public Car fetch(ParkingTicket ticket) throws UnrecogizedParkingTicketException {
        List<Car> fetchCarList = carsList.stream().filter(car -> car.getId() == ticket.getCar().getId()).collect(Collectors.toList());
        if (ticket.isUsed() || fetchCarList.isEmpty())
            throw new UnrecogizedParkingTicketException("Unrecognized parking ticket");
        ticket.setUsed(true);
        Car fetchCar = fetchCarList.get(0);
        carsList.remove(fetchCar);
        return fetchCar;
    }

    public boolean isCarParked(ParkingTicket ticket){
        for (Car car : carsList) {
            if (car.getId() == ticket.getCar().getId()) {
                return true;
            }
        }
        return false;
    }
}
