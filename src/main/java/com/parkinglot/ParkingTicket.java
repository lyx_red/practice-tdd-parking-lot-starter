package com.parkinglot;

public class ParkingTicket {
    private Car car;

    private boolean isUsed;

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }



    public Car getCar() {
        return car;
    }

    public ParkingTicket(Car car) {
        this.car = car;
    }

    public ParkingTicket(Car car, boolean isUsed) {
        this.car = car;
        this.isUsed = isUsed;
    }
}
