package com.parkinglot.exception;

public class UnrecogizedParkingTicketException extends RuntimeException{
    public UnrecogizedParkingTicketException(String message) {
        super(message);
    }
}
