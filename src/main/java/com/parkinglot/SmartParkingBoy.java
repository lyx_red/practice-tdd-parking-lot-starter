package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public class SmartParkingBoy extends ParkingBoy{
    private ParkingLot selectParkingLot;

    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    // find more position parking lot
    public ParkingLot findAvailableParkingLot() {
        ParkingLot[] parkingLots = getParkingLot();
        Optional<ParkingLot> parkingLotOptional = Arrays.stream(parkingLots).max((o1, o2) -> o1.getEmptyPosition() - o2.getEmptyPosition());
        selectParkingLot = parkingLotOptional.orElseGet(() -> new ParkingLot());
        return selectParkingLot;
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = findAvailableParkingLot();
        return availableParkingLot.park(car);
    }
}
