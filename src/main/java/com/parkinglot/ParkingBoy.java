package com.parkinglot;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecogizedParkingTicketException;

public class ParkingBoy {
    private final ParkingLot[] parkingLot;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLot = parkingLots;
    }

    public ParkingTicket park(Car car) {
        for (ParkingLot lot : parkingLot) {
            if (lot.getCarsList().size() < lot.getCapacity()) {
                return lot.park(car);
            }
        }
        throw new NoAvailablePositionException("No available position.");
    }

    public ParkingLot[] getParkingLot() {
        return parkingLot;
    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot lot : parkingLot) {
            if (lot.isCarParked(ticket)) {
                return lot.fetch(ticket);
            }
        }
        throw new UnrecogizedParkingTicketException("Unrecognized parking ticket");
    }
}
