package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public class SuperSmartParkingBoy extends ParkingBoy{
    private ParkingLot selectParkingLot;
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = findLargerAvailablePositionRate();
        return availableParkingLot.park(car);
    }

    private ParkingLot findLargerAvailablePositionRate() {
        ParkingLot[] parkingLots = getParkingLot();

        Optional<ParkingLot> max = Arrays.stream(parkingLots)
                .max(Comparator.comparingDouble(parkingLot -> ((double) parkingLot.getEmptyPosition() / parkingLot.getCapacity())));
        selectParkingLot = max.orElseGet(() -> new ParkingLot());
        return selectParkingLot;
    }
}
